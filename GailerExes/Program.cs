﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Diagnostics;
namespace GailerExes
{
    class Program
    {
        private static bool simpleVersion = true;

        //These booleans are if you want some of the more detailed examples to run in the command prompt
        //Or just read the code for your understanding and delete everything except lines 29 and 30 in the MAIN method
            private static bool showPathingTechniques = true;
            private static bool haltPriorToExit = true;

        static void Main(string[] args)
        {
            if (simpleVersion)
            {
                //Simple method...
                //START
                //◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊


                System.Diagnostics.Process.Start(@"C:\Program Files\Microsoft Office 15\root\office15\OUTLOOK.EXE");
                System.Diagnostics.Process.Start(@"C:\Program Files (x86)\eMailSignature\eMailSignature 365\emsclient.exe");

                //◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊◊
                //END
            }

            //
            //YOU COULD DELETE EVERYTHING AFTER THIS IF IT'S NOT SOMETHING YOU WANT TO TWEAK OR CUSTOMISE
            //It's included for your understanding and if you wanted something a bit more detailed
            //

            else
            {


                //*****************************************************************************************************
                //EXAMPLE OF HOW TO USE System.Diagnotics.Process in a more detailed fashion

                //Broken into sections
                Process myOutlookApp = new Process();
                myOutlookApp.StartInfo.FileName = @"C:\Program Files\Microsoft Office\Office15\OUTLOOK.EXE";
                //myOutlookApp.StartInfo.Arguments = "any paramaters?"
                myOutlookApp.Start();

                //Matt - this is your required filepath just past into the above
                //@"C:\Program Files\Microsoft Office 15\root\office15\OUTLOOK.EXE";

                //*****************************************************************************************************



                //*****************************************************************************************************
                //Matt - Uncomment this for your use
                // Process myEmailSignature = new Process();
                // myEmailSignature.StartInfo.FileName = @"C:\Program Files (x86)\eMailSignature\eMailSignature 365\emsclient.exe";
                // myEmailSignature.StartInfo.Arguments =
                // myEmailSignature.Start();

                //*****************************************************************************************************



                //----------------------------------------------------------------------------------------------------------------
                //Note the below section just show you how you can use more dynamic path variables
                if (showPathingTechniques)
                {
                    string myAPPDATA = Environment.GetEnvironmentVariable("APPDATA");  //like using %APPDATA%
                    Console.WriteLine(myAPPDATA);

                    string myCOMPUTERNAME = Environment.GetEnvironmentVariable("COMPUTERNAME");
                    Console.WriteLine(myCOMPUTERNAME);

                    //Standard call to ProgramFiles folder on x86 would be 
                        //Environment.GetEnvironmentVariable("ProgramFiles");

                    //On 64 Bit machines it breaks into these two
                        string myProgramFiles = Environment.GetEnvironmentVariable("ProgramW6432");
                        Console.WriteLine(myProgramFiles);

                        string myProgramFiles86 = Environment.GetEnvironmentVariable("ProgramFiles(x86)");
                        Console.WriteLine(myProgramFiles86);

                    Console.WriteLine("Press any key to continue...");
                    Console.ReadLine();


                    //Same as above outlook example but shows how you could use more dynamic path variables
                        Process myOpenAPathExample = new Process();
                        myOpenAPathExample.StartInfo.FileName = myProgramFiles + @"\Microsoft Office\Office15\OUTLOOK.EXE";
                        myOpenAPathExample.Start();

                    //You can in-line it all in one thing like this example (and the simple version up the top)
                        System.Diagnostics.Process.Start(Environment.GetEnvironmentVariable("windir") + @"\system32\notepad.exe", @"C:\Users\Kit\Desktop\PIA.txt");
                    //Or break it down (funtionally the same be somewhat more readable

                        string myWINDIR = Environment.GetEnvironmentVariable("windir");

                        Process myNotepadLoad = new Process();
                        myNotepadLoad.StartInfo.FileName = myWINDIR + @"\system32\notepad.exe";
                        myNotepadLoad.StartInfo.Arguments = @"C:\Users\Kit\Desktop\PIA.txt";
                        myNotepadLoad.Start();
                }

                //----------------------------------------------------------------------------------------------------------------
                //Note the below section can be ommited if you just want it to run 'silently' or set boolean above to true
                if (haltPriorToExit)
                {
                    Console.WriteLine("Press any key to close the application");
                    Console.ReadLine();
                }
                //----------------------------------------------------------------------------------------------------------------
            }
            
        }
    }
}
